﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApp.Data;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly MvcMovieContext _context;

        public HomeController(ILogger<HomeController> logger, MvcMovieContext context)
        {
            _context = context;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        //[Route("Infoclasses")]
        [HttpPost]
        public async Task<IActionResult> Logon(Accountcs acc)
        {
            //return View(nameof(Index));

            var result = _context.Accounts.Where(e => e.Username == acc.Username && e.Password == acc.Password).ToList();

            if (result.Count != 0)
            {
                return RedirectToAction("Index", "Infoclasses");
            }
            return RedirectToAction("Index");
        }
    }
}
