﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using WebApp.Data;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class InfoclassesController : Controller
    {
        private readonly MvcMovieContext _context;

        public InfoclassesController(MvcMovieContext context)
        {
            _context = context;
        }

        // GET: Infoclasses
        public async Task<IActionResult> Index()
        {
            return View(await _context.Book.ToListAsync());
        }

        // GET: Infoclasses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var infoclass = await _context.Book
                .FirstOrDefaultAsync(m => m.InfoId == id);
            if (infoclass == null)
            {
                return NotFound();
            }

            return View(infoclass);
        }

        // GET: Infoclasses/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Infoclasses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("InfoId,Name,Price")] Infoclass infoclass)
        {
            if (ModelState.IsValid)
            {
                _context.Add(infoclass);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(infoclass);
        }

        // GET: Infoclasses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var infoclass = await _context.Book.FindAsync(id);
            if (infoclass == null)
            {
                return NotFound();
            }
            return View(infoclass);
        }

        // POST: Infoclasses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("InfoId,Name,Price")] Infoclass infoclass)
        {
            if (id != infoclass.InfoId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(infoclass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InfoclassExists(infoclass.InfoId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(infoclass);
        }

        // GET: Infoclasses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var infoclass = await _context.Book
                .FirstOrDefaultAsync(m => m.InfoId == id);
            if (infoclass == null)
            {
                return NotFound();
            }

            return View(infoclass);
        }
        [HttpPost]
        public async Task<IActionResult> mySearch(string textSearch)
        {
            //return View(nameof(Index));
            var result = _context.Book.Where(b => b.Name.Contains(textSearch) || b.Price.ToString().Contains(textSearch)).ToList();
            if (string.IsNullOrEmpty(textSearch))
            {
                
                return RedirectToAction("Index");

            }
            else
            if (result.Count == 0)
            {
                return View("index", result);
            }
            else
            {
                return View("index", result);            //if (result.Count != 0)
            }
            //{
            //    return RedirectToAction("Index", "Infoclasses");
            //}
        }
        // POST: Infoclasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var infoclass = await _context.Book.FindAsync(id);
            _context.Book.Remove(infoclass);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InfoclassExists(int id)
        {
            return _context.Book.Any(e => e.InfoId == id);
        }
    }

}
