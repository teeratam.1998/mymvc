﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp.Data
{
    public class MvcMovieContext : DbContext
    {
        public MvcMovieContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Infoclass> Book { get; set; }
        public DbSet<Accountcs> Accounts { get; set; }
    }
}
