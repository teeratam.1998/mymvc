﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Infoclass
    {
        [Key]
        public int InfoId { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
    }
}
