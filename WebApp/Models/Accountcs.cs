﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Accountcs
    {
        [Key]
        public int AccountId { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
    }
}
